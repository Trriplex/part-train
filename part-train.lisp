(defpackage part-train
  (:use :cl :sdl2))
(in-package :part-train)

(defparameter *treble-key* t)



(declaim (inline is-valid))
(defun is-valid (key-val)
  "Checks if key-val is a c, d, e, f, g, a, or b"
  (or (= key-val 20) (and (< key-val 11) (> key-val 4))))

(defun load-images ()
  "Returns a vector with images from 1.bmp to 13.bmp"
  (let ((image-vec (make-array 13 :fill-pointer 0)))
    (dotimes (i 12)
      (vector-push
       (load-bmp (uiop:strcat "images/notes/" (write-to-string (1+ i)) ".bmp"))
       image-vec))
    image-vec))

(defun run ()
  (with-init (:video)
    (with-window (win :title "Partition reading training"
			   :w 331
			   :h 357
			   :flags '(:shown))
      (let ((current-image 6)
	    (surface (get-window-surface win))
	    (images (load-images))
	    (ok (load-bmp #p"images/ok.bmp"))
	    (notok (load-bmp #p"images/notok.bmp"))
	    (bass (load-bmp #p"images/bass.bmp"))
	    (treble (load-bmp #p"images/treble.bmp")))
	(with-event-loop (:method :poll)
	  (:quit () (format t "~a" (elt images 1)) t)
	  (:keydown (:keysym key)
		    (let ((key-val (scancode-value key)))
		      (if (is-valid key-val)
			  (if (check-key (scancode-value key) current-image)
			      (progn
				(blit-surface ok nil surface nil)
				(update-window win)
				(delay 1500)
				(setf current-image (random 13)))
			      (progn
				(blit-surface notok nil surface nil)
				(update-window win)
				(delay 1500)))
			  (when (= key-val 40)
			    (setf *treble-key* (not *treble-key*))
			    (if *treble-key*
				(blit-surface treble nil surface nil)
				(blit-surface bass nil surface nil))
			    (update-window win)
			    (delay 1500)))))
	(:idle ()
	       (blit-surface (elt images current-image) nil surface nil)
	       (update-window win)
	       (delay 100)))
      (loop for image across images do (free-surface image))
      (free-surface ok)
      (free-surface treble)
      (free-surface bass)
      (free-surface notok)))))

(defun check-key (key note)
  (if *treble-key*
      (case note
	((or 0 7) (= key 6))
	((or 1 8) (= key 7))
	((or 2 9) (= key 8))
	((or 3 10) (= key 9))
	((or 4 11) (= key 10))
	((or 12 5) (= key 20))
	(6 (= key 5)))
      (case note
	((or 5 12) (= key 6))
	(6 (= key 7))
	((or 0 7) (= key 8))
	((or 1 8) (= key 9))
	((or 2 9) (= key 10))
	((or 3 10) (= key 20))
	((or 4 11) (= key 5)))))
